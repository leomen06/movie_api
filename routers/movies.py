from fastapi import APIRouter
from fastapi import Path, Query, Depends#Path para validar rutas, y validacion Query
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field #librería perteneciente a FastAPI para crear el schema, y validaciones
from typing import Optional, List #para hacer opcional un parámetro.
from config.database import Session
from models.movie import Movie as MovieModel#renombramos para que tenga problemas con la otra clase Movie
from fastapi.encoders import jsonable_encoder#para pasar un objeto ddbb a JSONResponse
from middlewares.jwt_bearer import JWTBearer
from service.movie_service import MovieService
from schemas.movie_schema import Movie

movie_router = APIRouter()#creamos la instancia

@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:
	db = Session()
	result = MovieService(db).get_movies()
	return JSONResponse(status_code=200, content = jsonable_encoder(result))

@movie_router.get('/movie/{id}', tags=['movies'], response_model=Movie, status_code=200)
def get_movie(id: int = Path(ge=1, le=2000)) -> Movie:
	db = Session()
	result = MovieService(db).get_movie(id)#obtenemos el primer valor del filtrado
	if not result:#validación por si no hay result
		return JSONResponse(status_code = 404, content = {"message": "movie not found"})
	return JSONResponse(status_code=200, content = jsonable_encoder(result))

@movie_router.get('/movies/', tags=['movies'], response_model=List[Movie], status_code=200) #solo con la "/" detecta que debe ser un parámetro query
def get_movies_by_category(category: str = Query(min_length=5, max_length=20)) -> List[Movie]:#solicito 2 parámetros
	db = Session()
	result = MovieService(db).get_movie_by_category(category)
	if not result:#validación por si no hay result
		return JSONResponse(status_code = 404, content = {"message": "movies not found"})
	return JSONResponse(status_code=200, content = jsonable_encoder(result))
@movie_router.post('/movies', tags=['movies'], response_model=dict, status_code=201)
def create_movie(movie: Movie) -> dict:
	db = Session()
	MovieService(db).create_movie(movie)
	return JSONResponse(status_code=201, content={"message": "Movie created successfully"})

@movie_router.put('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)
def update_movie(id: int, movie: Movie) -> dict:
	db = Session()
	result = MovieService(db).get_movie(id)
	if not result:
		return JSONResponse(status_code = 404, content = {"message": "movie not found"})

	MovieService(db).update_movie(id, movie)
	return JSONResponse(status_code=200, content={"message": "Movie updated successfully"})

@movie_router.delete('/movie/{id}', tags=['movies'], response_model=dict, status_code=200)
def delete_movie(id: int = Path(ge=1, le=2000)) -> dict:
	db = Session()
	result = MovieService(db).get_movie(id)
	if not result:
		JSONResponse(status_code = 404, content = {"message": "movie not found"})
	MovieService(db).delete_movie(id)
	return JSONResponse(status_code=200, content={"message": "Movie deleted successfully"})
