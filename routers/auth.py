from fastapi import APIRouter
from utils.jwt_manager import create_token
from fastapi.responses import JSONResponse
from schemas.user_schema import User

auth_router = APIRouter()#instanciamos

@auth_router.post('/login', tags=['auth'])
def login(user: User):
	if user.email == "admin@gmail.com" and user.password == "admin":
		token: str = create_token(user.dict()) #lo guardo como dict
	return JSONResponse(status_code=200, content=token)
