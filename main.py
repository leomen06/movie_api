from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from pydantic import BaseModel #librería perteneciente a FastAPI para crear el schema, y validaciones
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movies import movie_router
from routers.auth import auth_router
app = FastAPI()

app.title = "Cambiando título"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(auth_router)

Base.metadata.create_all(bind=engine)

@app.get('/', tags=['message'], status_code=200)
def message():
    return HTMLResponse('<h1> Hello Word</h1>')



