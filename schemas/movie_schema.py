from pydantic import BaseModel, Field #librería perteneciente a FastAPI para crear el schema, y validaciones
from typing import Optional, List #para hacer opcional un parámetro.

class Movie(BaseModel):#utilizamos el constructor de la clase BaseModel
	id: Optional[int] = None#entero opcional, None por default
	title: str = Field(min_length=5, max_length=25)
	overview: str = Field(min_length=5, max_length=50)
	year: int = Field(le=2022)#le --> less than or equal
	rating: float = Field(le=10)
	category: str = Field(min_length=5, max_length=25)

	class Config:
		schema_extra = {
			"example": {
				"title": "insert_title",
				"overview": "insert_overview",
				"year": 2022,
				"rating": 5.5,
				"category": "insert_category"
			}
		}