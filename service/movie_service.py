from models.movie import Movie as MovieModel
from schemas.movie_schema import Movie

class MovieService():

    def __init__(self, db) -> None:#se requiere que cada vez que se inicie el servicio se le envíe una sesión de base de datos.
        self.db = db# a self.db le asigno lo que llega como db. Hasta aquí tengo acceso a esta db para que pueda ser accesible en otros métodos 
        #de este servicio

#creamos el primer método/servicio para retornar las películas:
    def get_movies(self):
        result = self.db.query(MovieModel).all()
        return result
    
    def get_movie(self, id):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        return result

    def get_movie_by_category(self, category):
        result = self.db.query(MovieModel).filter(MovieModel.category == category).all()
        return result
    
    def create_movie(self, movie: Movie):
        new_movie = MovieModel(**movie.dict())
        self.db.add(new_movie)
        self.db.commit()
        return

    def update_movie(self, id: int, movie: Movie):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        result.title = movie.title
        result.overview = movie.overview
        result.year = movie.year
        result.rating = movie.rating
        result.category = movie.category
        self.db.commit()
        return 
    
    def delete_movie(self, id: int):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        self.db.delete(result)
        self.db.commit()
        return
